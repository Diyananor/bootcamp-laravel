@extends('layout.public')

@section('content')
    {{-- <div>
        {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
    </div> --}}

    @if (session('status'))
        <div>
            {{ session('status') }}
        </div>
    @endif

    @if ($errors->any())
        <div>
            <div>{{ __('Whoops! Something went wrong.') }}</div>

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        {{-- <div>
            <label>{{ __('Email') }}</label>
            <input type="email" name="email" value="{{ old('email') }}" required autofocus />
        </div>

        <div>
            <button type="submit">
                {{ __('Email Password Reset Link') }}
            </button>
        </div> --}}

        <div class="form-group row">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                    <p class="mb-4">We get it, stuff happens. Just enter your email address below
                        and we'll send you a link to reset your password!</p>
                </div>
                <form class="user">
                    <div class="form-group">
                        <input type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                    </div>
                    <a href="/login" class="btn btn-primary btn-user btn-block">
                        Reset Password
                    </a>
                </form>
                <hr>
                <div class="text-center">
                    <a class="small" href="/register">Create an Account!</a>
                </div>
                <div class="text-center">
                    <a class="small" href="/login">Already have an account? Login!</a>
                </div>
            </div>
        </div>
    </form>
@endsection

