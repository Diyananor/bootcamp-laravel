@extends('layout.public')

@section('content')
    @if (session('status'))
        <div>
            {{ session('status') }}
        </div>
    @endif

    @if ($errors->any())
        <div>
            <div>{{ __('Whoops! Something went wrong.') }}</div>

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" class="user" action="{{ route('login') }}">
        @csrf

        <div class="form-group">
            <label>{{ __('Email') }}</label>
            <input type="email" name="email" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Enter Email Address..." value="{{ old('email') }}" required autofocus />
        </div>

        <div class="form-group">
            <label>{{ __('Password') }}</label>
            <input type="password" name="password" class="form-control form-control-user" placeholder="Password" required autocomplete="current-password" placeholder="Password" />
        </div>

        {{-- <div class="form-group">
            <label>{{ __('Remember me') }}</label>
            <input type="checkbox" name="remember">
        </div> --}}
        <div class="form-group">
            <div class="custom-control custom-checkbox small">
                <input type="checkbox" class="custom-control-input" name="remember" id="customCheck">
                <label class="custom-control-label" for="customCheck">Remember
                    Me</label>
            </div>
        </div>

        <div>
            <button type="submit" class="btn btn-primary btn-user btn-block">
               {{ __('Login') }}
            </button>
        </div>

        <hr>
    
    <div class="text-center">
        <a class="small" href="/register">Create an Account!</a>
    </div>

        @if (Route::has('password.request'))
            
            <div class="text-center">
                <a class="small" href="{{ route('password.request') }}">
                    {{ __('Forgot your password?') }}
                </a>
                {{-- <a class="small" href="forgot-password.html">Forgot Password?</a> --}}
            </div>
        @endif

    </form>
@endsection
