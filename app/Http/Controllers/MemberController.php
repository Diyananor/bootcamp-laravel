<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    //--- /member
    public function index(){

        $user = Auth::user();

        return view('dashboard');
    }

    //--- /profile
    public function profile(){
        return view('member.profile');
    }

    //--- /member/login
    public function login(){
        return view('member.login');
    }

    //--- /member/register
    public function register(){
        return view('member.register');
    }
}
